package se.stjerneman.common.user;

import java.io.Serializable;

import javax.swing.ImageIcon;

/**
 * 
 * @author Emil Stjerneman
 * 
 */
public class ClientUser implements Serializable {

    private static final long serialVersionUID = 2645813330641002270L;

    /**
     * The username of this client.
     */
    private String username;

    /**
     * This users image.
     */
    private ImageIcon image;

    /**
     * This users ip address.
     */
    private String ip;

    /**
     * Gets this users username.
     * 
     * @return this users username.
     */
    public String getUsername () {
        return username;
    }

    /**
     * Sets this users username.
     * 
     * @param username
     *            the new username.
     */
    public void setUsername (String username) {
        this.username = username;
    }

    /**
     * Gets this users image.
     * 
     * @return this users image.
     */
    public ImageIcon getImage () {
        return image;
    }

    /**
     * Sets this users image.
     * 
     * @param image
     *            this users image.
     */
    public void setImage (ImageIcon image) {
        this.image = image;
    }

    /**
     * Gets this users ip.
     * 
     * @return this users ip.
     */
    public String getIP () {
        return ip;
    }

    /**
     * Sets this users ip.
     * 
     * @param ip
     *            the new ip.
     */
    public void setIP (String ip) {
        this.ip = ip;
    }

    @Override
    public String toString () {
        return getUsername();
    }
}
