package se.stjerneman.server.properties;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Handle server properties.
 * 
 * Default properties will be overridden by the same properties in the property
 * file.
 * 
 * @author Emil Stjerneman
 * 
 */
public class ServerProperties {

    private static final Logger logger = Logger
            .getLogger(ServerProperties.class.getName());

    /**
     * A property list that contains server properties.
     */
    private static Properties properties;

    /**
     * The file that could contain properties.
     */
    private static File propertyFile;

    /**
     * 
     */
    public ServerProperties () {
        propertyFile = new File("config.properties");
        initProperties();
    }

    /**
     * Searches for the property with the specified key in this property list.
     * 
     * @param key
     *            the property key.
     * @return the value in this property list with the specified key value.
     */
    public String getProperty (String key) {
        return ServerProperties.properties.getProperty(key);
    }

    /**
     * Initialize properties.
     */
    private void initProperties () {
        ServerProperties.properties = new Properties();
        setDefaultPoperties();

        if (propertyFile.exists()) {
            loadPropertiesFromFile();
        }
        else {
            saveSettings();
        }
    }

    /**
     * Reads a property list from the property file.
     */
    private void loadPropertiesFromFile () {
        try {
            ServerProperties.properties.load(new FileReader(propertyFile));
            logger.info("Properties from config.properties loaded.");
        }
        catch (IOException e) {
            logger.error("Couldn't load the config.properties file.", e);
        }
    }

    /**
     * Stores this property list to a property file.
     */
    private void saveSettings () {
        try {
            ServerProperties.properties.store(new FileWriter(propertyFile),
                    "AnonChat server config properties.");
            logger.info("Properties saved to config.properties.");
        }
        catch (IOException e) {
            logger.error("Couldn't save the config.properties file.", e);
        }
    }

    /**
     * Sets default properties.
     */
    private void setDefaultPoperties () {
        ServerProperties.properties.setProperty("port", "27015");
        ServerProperties.properties.setProperty("backlog", "100");
    }
}
