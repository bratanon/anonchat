package se.stjerneman.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import se.stjerneman.common.messages.Message;
import se.stjerneman.common.messages.ServerMessage;
import se.stjerneman.common.messages.SignInMessage;
import se.stjerneman.common.messages.SignOutMessage;
import se.stjerneman.common.user.ClientUser;
import se.stjerneman.server.properties.ServerProperties;

/**
 * A TCP server that clients can connect to.
 * 
 * @author Emil Stjerneman
 * 
 */
public class TCPServer {

    private static final Logger logger = Logger.getLogger(TCPServer.class
            .getName());
    /**
     * A TCPServer instance.
     */
    private static TCPServer instance = null;

    /**
     * A list of properties this server has.
     */
    private ServerProperties properties;

    /**
     * This TCPServers socket.
     */
    private ServerSocket serverSocket;

    /**
     * A list of connected client.
     */
    private HashMap<String, ClientListener> clients;

    /**
     * A list of connected client sockets.
     */
    private HashMap<String, Socket> clientSockets;

    /**
     * TCPServer entry point.
     * 
     * @param args
     */
    public static void main (String[] args) {
        BasicConfigurator.configure();
        TCPServer.getInstance().startServer();
    }

    /**
     * Gets an instance of this TCPServer if it exists, otherwise an instance is
     * created.
     * 
     * @return an instance of a TCPServer.
     */
    public synchronized static TCPServer getInstance () {
        if (TCPServer.instance == null) {
            TCPServer.instance = new TCPServer();
        }

        return TCPServer.instance;
    }

    /**
     * Sends an object to all connected clients.
     * 
     * @param object
     *            the object to send to all clients.
     */
    // TODO: Fix the object argument.
    protected synchronized void broadcast (Object object, byte byteToSend) {
        for (Entry<String, ClientListener> entry : clients.entrySet()) {
            entry.getValue().send(object, byteToSend);
        }
    }

    /**
     * Sends a message to all connected clients.
     * 
     * @param message
     *            the message to send to all clients.
     */
    protected synchronized void broadcastMessage (Message message) {
        broadcast(message, (byte) 1);
    }

    /**
     * Sends a UserList object to all clients.
     */
    protected synchronized void broadcastUserList () {
        List<ClientUser> clientNames = new ArrayList<ClientUser>();
        for (Entry<String, ClientListener> entry : clients.entrySet()) {
            clientNames.add(entry.getValue().getClientUser());
        }

        broadcast(clientNames, (byte) 2);
    }

    /**
     * Disconnects a client by closing its streams.
     * 
     * @param uuid
     *            the client UUID.
     */
    protected synchronized void disconnectClient (String uuid) {
        try {
            Socket clientSocket = clientSockets.get(uuid);
            if (clientSocket != null) {
                clientSocket.shutdownOutput();
                clientSocket.shutdownInput();
                clientSocket.close();

                removeClient(uuid);
            }
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Removes a client from the lists.
     * 
     * @param uuid
     *            the client UUID.
     */
    protected synchronized void removeClient (String uuid) {
        String username = clients.get(uuid).getClientUser().getUsername();
        this.broadcastMessage(new SignOutMessage(username));

        clients.remove(uuid);
        clientSockets.remove(uuid);

        this.broadcastUserList();
    }

    /**
     * Starts the server.
     */
    private void startServer () {
        properties = new ServerProperties();

        clients = new HashMap<String, ClientListener>();
        clientSockets = new HashMap<String, Socket>();

        createServerSocket();
        startServerTalk();
        while (true) {
            connectionListener();
        }
    }

    /**
     * Listens for a connection to be made to this socket and accepts it.
     */
    private void connectionListener () {
        try {
            Socket clientSocket = this.serverSocket.accept();

            String clientHost = clientSocket.getInetAddress().getHostAddress();
            logger.info("Connection accepted from " + clientHost);

            createNewClientListenrer(clientSocket);
        }
        catch (IOException | SecurityException e) {
            logger.error("Receiving connection failed.", e);
        }
    }

    /**
     * Creates a new thread for the incoming connection.
     * 
     * @param clientSocket
     *            A new Socket for this client.
     */
    private synchronized void createNewClientListenrer (Socket clientSocket) {
        try {
            String uuid = UUID.randomUUID().toString();

            clientSockets.put(uuid, clientSocket);

            ClientListener clientListener = new ClientListener(uuid,
                    clientSocket);
            clients.put(uuid, clientListener);

            Thread clientThread = new Thread(clientListener);

            clientThread.setName("Client " + uuid);
            clientThread.start();

            if (validateUserName(clientListener)) {
                String username = clientListener.getClientUser().getUsername();
                broadcastMessage(new SignInMessage(username));
                broadcastUserList();
            }
            else {
                this.disconnectClient(uuid);
            }
        }
        catch (IllegalArgumentException e) {
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
            logger.error("Couldn't create new clientListener.", e);
        }
    }

    /**
     * Creates a server socket that clients can connection to.
     */
    private void createServerSocket () {
        try {
            int port = Integer.parseInt(properties.getProperty("port"));
            int backlog = Integer.parseInt(properties.getProperty("backlog"));

            serverSocket = new ServerSocket(port, backlog);

            logger.info("Server started.");
            logger.info("Port : " + port);
            logger.info("backlog : " + backlog);
            logger.info("IP : " + serverSocket.getInetAddress());
        }
        catch (IOException | SecurityException e) {
            logger.error("Couldn't open server socket.", e);
        }
    }

    /**
     * Validate a username.
     * 
     * @param clientListener
     *            the clientListener to validate the username of.
     * 
     * @return true if the username is valid, otherwise false.
     */
    private synchronized boolean validateUserName (ClientListener clientListener) {
        String username = clientListener.getClientUser().getUsername()
                .toLowerCase();
        String uuid = clientListener.getUuid();

        List<String> disallowed = Arrays.asList("server", "admin", "mod");

        if (disallowed.contains(username)) {
            clientListener.sendUserNameValidationStatus(false);
            return false;
        }

        for (Entry<String, ClientListener> entry : clients.entrySet()) {
            String name = entry.getValue().getClientUser().getUsername()
                    .toLowerCase();

            boolean self = (entry.getValue().getUuid() == uuid);
            if (!self && name.equals(username)) {
                clientListener.sendUserNameValidationStatus(false);
                return false;
            }
        }

        clientListener.sendUserNameValidationStatus(true);
        return true;
    }

    /**
     * Starts a new thread that will read messages that the server writes and
     * broadcast them to one or more clients.
     */
    private void startServerTalk () {
        new Thread(new ServerTalk()).start();
    }

    /**
     * Provide the server with the possibility to talk to all clients.
     * 
     * @author Emil Stjerneman
     * 
     */
    private class ServerTalk implements Runnable {
        /**
         * Stream to read text from.
         */
        private BufferedReader input;

        @Override
        public void run () {
            try {
                readInput();
            }
            catch (IOException e) {
                logger.error("Server input error.", e);
            }
            finally {
                closeInput();
            }
        }

        /**
         * Reads what the server types in the console.
         * 
         * @throws IOException
         *             If an I/O error occurs
         */
        private void readInput () throws IOException {
            input = new BufferedReader(new InputStreamReader(System.in));

            String message = null;
            while ((message = input.readLine()) != null) {
                broadcast(new ServerMessage(message), (byte) 1);
            }
        }

        /**
         * Closes the input stream.
         */
        private void closeInput () {
            try {
                input.close();
            }
            catch (IOException e) {}
        }
    }
}
