package se.stjerneman.server;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import org.apache.log4j.Logger;

import se.stjerneman.common.messages.Message;
import se.stjerneman.common.user.ClientUser;

/**
 * 
 * @author Emil Stjerneman
 * 
 */
public class ClientListener implements Runnable {

    private static final Logger logger = Logger.getLogger(ClientListener.class
            .getName());

    /**
     * The TCPServer to work with.
     */
    private TCPServer server = TCPServer.getInstance();

    /**
     * The user for this listener.
     */
    private ClientUser clientUser;

    /**
     * This client socket.
     */
    private Socket clientSocket;

    /**
     * Stream to receive data from the client.
     */
    private ObjectInputStream input;

    /**
     * Stream to send data to the client.
     */
    private ObjectOutputStream output;

    /**
     * This threads UUID.
     */
    private final String uuid;

    /**
     * 
     * @param uuid
     *            the uuid for this client.
     * @param clientSocket
     *            this client socket.
     * @throws IOException
     */
    public ClientListener (String uuid, Socket clientSocket) throws IOException {
        this.uuid = uuid;
        this.clientSocket = clientSocket;

        setupStreams();
        getClientUserFromClient();
    }

    /**
     * Runs the thread and wait for client input.
     */
    @Override
    public void run () {
        try {
            while (true) {
                String text = this.input.readObject().toString();

                String username = clientUser.getUsername();

                if (Command.isCommand(text)) {
                    Command.process(text, username);
                    continue;
                }

                server.broadcastMessage(new Message(username, text));
            }
        }
        catch (ClassNotFoundException e) {
            logger.fatal("This should neven happen.", e);
        }
        catch (EOFException e) {}
        catch (SocketException e) {}
        catch (SocketTimeoutException e) {
            logger.error("Socket timed out.", e);
        }
        catch (IOException e) {
            logger.error("IOException when reading from client.", e);
        }
        finally {
            logger.info("Client disconnected.");
            server.disconnectClient(uuid);
        }
    }

    /**
     * Gets the user for this client.
     * 
     * @return the user for this client.
     */
    public ClientUser getClientUser () {
        return clientUser;
    }

    /**
     * Sets the user for this client.
     * 
     * @param clientUser
     *            the user object to set as this clients user.
     */
    public void setClientUser (ClientUser clientUser) {
        this.clientUser = clientUser;
    }

    /**
     * Gets the socket for this client.
     * 
     * @return the socket for this client.
     */
    public Socket getClientSocket () {
        return clientSocket;
    }

    /**
     * Gets this clients uuid.
     * 
     * @return the uuid for this client.
     */
    public String getUuid () {
        return uuid;
    }

    /**
     * Writes a message to the client output stream.
     * 
     * @param message
     *            a message to send to the client.
     */
    protected void send (Object object, byte byteToSend) {
        try {
            if (!clientSocket.isOutputShutdown()) {
                output.writeByte(byteToSend);
                output.writeObject(object);
            }
        }
        catch (IOException e) {
            logger.error("IOException when writing to client.");
            // TODO: Close socket here, and remove it from the lists on the
            // server. Notice, this is called when sending out disconnected
            // messages to all clients.
        }
    }

    /**
     * Creates an input and output stream.
     * 
     * @throws IOException
     *             if an I/O error occurs when initializing the streams.
     */
    private void setupStreams () throws IOException {
        try {
            setupOutputStream();
            setupInputStream();
        }
        catch (IOException e) {
            throw e;
        }
    }

    /**
     * Initialize a stream to send data to the client.
     * 
     * @throws IOException
     *             if an I/O error occurs when getting the output stream.
     */
    private void setupOutputStream () throws IOException {
        output = new ObjectOutputStream(clientSocket.getOutputStream());
    }

    /**
     * Initialize a stream to receive data from the client.
     * 
     * @throws IOException
     *             if an I/O error occurs when getting the input stream.
     */
    private void setupInputStream () throws IOException {
        input = new ObjectInputStream(clientSocket.getInputStream());
    }

    /**
     * 
     * @throws IOException
     */
    private void getClientUserFromClient () throws IOException {
        try {
            setClientUser((ClientUser) input.readObject());
            clientUser.setIP(clientSocket.getInetAddress().toString());
        }
        catch (ClassNotFoundException e) {
            // Will not happen.
        }
        catch (IOException e) {
            logger.error("Couldn't get the user object.", e);
            throw e;
        }
    }

    /**
     * Sends a boolean that represent the validation status on the username.
     */
    protected void sendUserNameValidationStatus (boolean status) {
        try {
            output.writeBoolean(status);
        }
        catch (IOException e) {}
    }
}
