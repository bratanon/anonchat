package se.stjerneman.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import se.stjerneman.common.user.ClientUser;

/**
 * 
 * @author Emil Stjerneman
 * 
 */
public class Client {

    private static final Logger logger = Logger.getLogger(Client.class
            .getName());

    /**
     * A client instance.
     */
    private static Client instance = null;

    /**
     * This clients socket.
     */
    private Socket socket;

    /**
     * Stream to receive data from the user.
     */
    private ObjectInputStream input;

    /**
     * Stream to send data to the server.
     */
    private ObjectOutputStream output;

    /**
     * Gets an instance of the client.
     * 
     * @return an instance of the client.
     */
    public static Client getInstance () {
        if (Client.instance == null) {
            Client.instance = new Client();
        }
        return Client.instance;
    }

    /**
     * Gets this clients socket.
     * 
     * @return this clients socket.
     */
    public synchronized Socket getSocket () {
        return socket;
    }

    /**
     * Gets the input stream for this client.
     * 
     * @return the input stream for this client.
     */
    public synchronized ObjectInputStream getInputStream () {
        return input;
    }

    /**
     * Gets the output stream for this client.
     * 
     * @return the output stream for this client.
     */
    public synchronized ObjectOutputStream getOutputStream () {
        return output;
    }

    /**
     * Connects this client to the host.
     * 
     * @param hostIpAddress
     *            the IP address of the host to connect to.
     * @param hostPort
     *            the port the host is listening to.
     * @throws UnknownHostException
     *             if the IP address of the host could not be determined.
     * @throws IOException
     *             if an I/O error occurs when creating the socket.
     */
    public void connect (String hostIpAddress, int hostPort)
            throws UnknownHostException, IOException {
        try {
            logger.info("Client trying to connect to host.");
            connectToHost(hostIpAddress, hostPort);
            setupStreams();
        }
        catch (UnknownHostException e) {
            logger.warn("Unknown host.");
            throw e;
        }
        catch (IOException e) {
            throw e;
        }
    }

    /**
     * Disconnects this client.
     */
    public void disconnect () {
        try {
            if (socket != null) {
                socket.shutdownOutput();
                socket.shutdownInput();
                socket.close();
                logger.info("Closed socket.");
            }
        }
        catch (IOException e) {}
    }

    /**
     * Sends a message to the server.
     * 
     * @param message
     *            a message to send to the server.
     */
    public void sendMessage (String message) {
        try {
            output.writeObject(message);
        }
        catch (IOException e) {
            logger.error("Couldn't send message to server.", e);
        }
    }

    /**
     * Sends a client user object to the server.
     * 
     * @param user
     *            a client user object.
     */
    public void sendUser (ClientUser user) {
        try {
            output.writeObject(user);
            if (!getValidationStatus()) {
                throw new IllegalArgumentException(
                        "The username is invalid or already in use.");
            }
        }
        catch (IOException e) {
            logger.error("Couldn't send the user to server.", e);
        }
    }

    /**
     * @return true when the username validation is ok, otherwise false.
     */
    public boolean getValidationStatus () {
        try {
            return input.readBoolean();
        }
        catch (IOException e) {}

        return false;
    }

    /**
     * Connects to the host.
     * 
     * @throws UnknownHostException
     *             if the IP address of the host could not be determined.
     * @throws IOException
     *             if an I/O error occurs when creating the socket.
     */
    private void connectToHost (String hostIpAddress, int hostPort)
            throws IOException {
        try {
            // TODO: SET TIME OUT!
            socket = new Socket(hostIpAddress, hostPort);
            logger.info("Client connected to host.");
        }
        catch (UnknownHostException e) {
            throw e;
        }
        catch (IOException e) {
            throw e;
        }
    }

    /**
     * Creates an input and output stream.
     * 
     * @throws IOException
     *             if an I/O error occurs when initializing the streams.
     */
    private void setupStreams () throws IOException {
        try {
            setupOutputStream();
            setupInputStream();
        }
        catch (IOException e) {
            logger.error("Can't initialize streams.", e);
            throw e;
        }
    }

    /**
     * Initialize a stream to send data to the server.
     * 
     * @throws IOException
     *             if an I/O error occurs when getting the output stream.
     */
    private void setupOutputStream () throws IOException {
        output = new ObjectOutputStream(socket.getOutputStream());
    }

    /**
     * Initialize a stream to receive data from the client.
     * 
     * @throws IOException
     *             if an I/O error occurs when getting the input stream.
     */
    private void setupInputStream () throws IOException {
        input = new ObjectInputStream(socket.getInputStream());
    }
}
