package se.stjerneman.client.ui.utils;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

/**
 * Handle window application icons.
 * 
 * @author Emil Stjerneman
 * 
 */
public class ApplicationIcons {

    /**
     * Gets a list of icons to use as window or dialog icons.
     * 
     * @return a list of icons.
     */
    public static List<Image> getIcons () {
        List<Image> imageList = new ArrayList<>();

        Toolkit tk = Toolkit.getDefaultToolkit();

        imageList
                .add(tk.getImage(ApplicationIcons.class
                        .getResource("/se/stjerneman/client/ui/icons/chatIcon128.png")));
        imageList
                .add(tk.getImage(ApplicationIcons.class
                        .getResource("/se/stjerneman/client/ui/icons/chatIcon64.png")));
        imageList
                .add(tk.getImage(ApplicationIcons.class
                        .getResource("/se/stjerneman/client/ui/icons/chatIcon32.png")));
        imageList
                .add(tk.getImage(ApplicationIcons.class
                        .getResource("/se/stjerneman/client/ui/icons/chatIcon16.png")));

        return imageList;
    }
}
