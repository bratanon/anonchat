package se.stjerneman.client.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import se.stjerneman.client.Client;

/**
 * 
 * @author Emil Stjerneman
 * 
 */
public class ClientWindowMenu {
    private static final ResourceBundle LANGUAGE = ResourceBundle
            .getBundle("se.stjerneman.client.ui.lang"); //$NON-NLS-1$

    private ClientWindow clientWindow;

    private final JMenuBar menuBar = new JMenuBar();
    private JMenu mnChat = new JMenu(
            LANGUAGE.getString("ClientWindowMenu.mnChat.text")); //$NON-NLS-1$
    private JMenu mnView = new JMenu(
            LANGUAGE.getString("ClientWindowMenu.mnView.text")); //$NON-NLS-1$
    private JMenu mnHelp = new JMenu(
            LANGUAGE.getString("ClientWindowMenu.mnHelp.text")); //$NON-NLS-1$

    private JMenuItem mntmConnect = new JMenuItem(
            LANGUAGE.getString("ClientWindowMenu.mntmConnect.text")); //$NON-NLS-1$
    private JMenuItem mntmDisconnect = new JMenuItem(
            LANGUAGE.getString("ClientWindowMenu.mntmDisconnect.text")); //$NON-NLS-1$
    private JMenuItem mntmSettings = new JMenuItem(
            LANGUAGE.getString("ClientWindowMenu.mntmSettings.text")); //$NON-NLS-1$
    private JMenuItem mntmExit = new JMenuItem(
            LANGUAGE.getString("ClientWindowMenu.mntmExit.text")); //$NON-NLS-1$

    private JCheckBoxMenuItem mntmShowUsers = new JCheckBoxMenuItem(
            LANGUAGE.getString("ClientWindowMenu.mntmShowUsers.text")); //$NON-NLS-1$

    private JMenuItem mntmAboutChat = new JMenuItem(
            LANGUAGE.getString("ClientWindowMenu.mntmAboutChat.text")); //$NON-NLS-1$
    private JMenuItem mntmLicense = new JMenuItem(
            LANGUAGE.getString("ClientWindowMenu.mntmLicense.text")); //$NON-NLS-1$

    @SuppressWarnings ("javadoc")
    public ClientWindowMenu (ClientWindow cw) {
        clientWindow = cw;

        menuBar.add(mnChat);
        menuBar.add(mnView);
        menuBar.add(mnHelp);

        mnChat.add(mntmConnect);
        mnChat.add(mntmDisconnect);
        mnChat.addSeparator();
        mnChat.add(mntmSettings);
        mnChat.addSeparator();
        mnChat.add(mntmExit);

        mnView.add(mntmShowUsers);

        mnHelp.add(mntmLicense);
        mnHelp.add(mntmAboutChat);

        mntmShowUsers.setSelected(true);
        mntmDisconnect.setVisible(false);

        mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
                InputEvent.CTRL_MASK));

        mntmConnect.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent event) {
                ConnectDialog cd = new ConnectDialog(clientWindow);
                cd.setVisible(true);
            }
        });

        mntmDisconnect.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent event) {
                Client.getInstance().disconnect();
                setDisconnectedMode();

                clientWindow.getClientListModel().clear();
            }
        });

        mntmExit.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent event) {
                System.exit(0);
            }
        });

        mntmLicense.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent event) {
                LicenseDialog ld = new LicenseDialog();
                ld.setVisible(true);
                ld.pack();
            }
        });

        mntmAboutChat.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent event) {
                new AboutDialog().setVisible(true);
            }
        });

        mntmShowUsers.addItemListener(new ItemListener() {
            public void itemStateChanged (ItemEvent e) {
                clientWindow.getClientListScrollPane().setVisible(
                        mntmShowUsers.isSelected());
            }
        });

        mntmSettings.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent event) {
                ClientSettingsDialog sd = new ClientSettingsDialog(clientWindow);
                sd.setVisible(true);
                sd.pack();
            }
        });
    }

    @SuppressWarnings ("javadoc")
    public JMenuBar getMenuBar () {
        return menuBar;
    }

    protected void setDisconnectedMode () {
        mntmConnect.setVisible(true);
        mntmDisconnect.setVisible(false);
        clientWindow.getChatLogTextPane().setEnabled(false);
        clientWindow.getMessageTextArea().setEnabled(false);
        clientWindow.getMessageTextArea().setEditable(false);
        clientWindow.getBtnSend().setEnabled(false);
    }

    protected void setConnectedMode () {
        mntmConnect.setVisible(false);
        mntmDisconnect.setVisible(true);
        clientWindow.getChatLogTextPane().setEnabled(true);
        clientWindow.getMessageTextArea().setEnabled(true);
        clientWindow.getMessageTextArea().setEditable(true);
        clientWindow.getBtnSend().setEnabled(true);
    }
}
