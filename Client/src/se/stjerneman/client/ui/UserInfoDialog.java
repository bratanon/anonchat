package se.stjerneman.client.ui;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

import se.stjerneman.common.user.ClientUser;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import java.util.ResourceBundle;

/**
 * Show user information in a dialog.
 * 
 * @author Emil Stjerneman
 * 
 */
public class UserInfoDialog extends JDialog {
    private static final ResourceBundle LANGUAGE = ResourceBundle.getBundle("se.stjerneman.client.ui.lang"); //$NON-NLS-1$

    private static final long serialVersionUID = -142279400676266093L;

    /**
     * Create the dialog.
     * 
     * @param user
     *            the current client user to view info on.
     */
    public UserInfoDialog (ClientUser user) {
        setModalityType(ModalityType.APPLICATION_MODAL);
        setBounds(100, 100, 200, 296);
        setMinimumSize(new Dimension(200, 200));
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setTitle(LANGUAGE.getString("UserInfoDialog.this.title")); //$NON-NLS-1$
        getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
                FormFactory.RELATED_GAP_COLSPEC,
                FormFactory.DEFAULT_COLSPEC,
                FormFactory.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"),
                FormFactory.RELATED_GAP_COLSPEC, },
                new RowSpec[] {
                        FormFactory.RELATED_GAP_ROWSPEC,
                        RowSpec.decode("fill:default:grow"),
                        FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, }));

        JLabel placeholderImage = new JLabel("");
        if (user.getImage() != null) {
            placeholderImage.setIcon(user.getImage());
        }
        else {
            placeholderImage.setIcon(new ImageIcon(UserInfoDialog.class
                    .getResource("/assets/user-image-default.png")));
        }
        getContentPane().add(placeholderImage, "2, 2, 3, 1");

        JLabel lblUsername = new JLabel(LANGUAGE.getString("UserInfoDialog.lblUsername.text")); //$NON-NLS-1$
        getContentPane().add(lblUsername, "2, 4");

        JLabel placeholderUsername = new JLabel("");
        placeholderUsername.setText(user.getUsername());
        getContentPane().add(placeholderUsername, "4, 4");

        JLabel lblIp = new JLabel(LANGUAGE.getString("UserInfoDialog.lblIp.text")); //$NON-NLS-1$
        getContentPane().add(lblIp, "2, 6");

        JLabel placeholderIP = new JLabel("");
        placeholderIP.setText(user.getIP());
        getContentPane().add(placeholderIP, "4, 6");
    }
}
