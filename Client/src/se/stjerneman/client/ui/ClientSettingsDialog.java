package se.stjerneman.client.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author Emil
 * 
 */
public class ClientSettingsDialog extends JDialog {
    private static final ResourceBundle LANGUAGE = ResourceBundle
            .getBundle("se.stjerneman.client.ui.lang"); //$NON-NLS-1$

    private ClientWindow clientWindow;

    private final JPanel contentPanel = new JPanel();
    private final Action saveSettingsAction = new SaveSettingsAction();
    private JComboBox<String> languageBox;
    private final Action cancelAction = new CancelAction();

    /**
     * Create the dialog.
     */
    public ClientSettingsDialog (ClientWindow clientWindow) {

        this.clientWindow = clientWindow;
        setTitle(LANGUAGE.getString("ClientSettingsDialog.this.title")); //$NON-NLS-1$
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new FormLayout(new ColumnSpec[] {
                FormFactory.RELATED_GAP_COLSPEC,
                FormFactory.DEFAULT_COLSPEC,
                FormFactory.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"), },
                new RowSpec[] {
                        FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, }));
        {
            JLabel lblLanguage = new JLabel(
                    LANGUAGE.getString("ClientSettingsDialog.lblLanguage.text")); //$NON-NLS-1$
            contentPanel.add(lblLanguage, "2, 2, right, default");
        }
        {
            languageBox = new JComboBox<String>();
            languageBox.setModel(new DefaultComboBoxModel<String>(new String[] {
                    "en",
                    "sv" }));

            String lang = clientWindow.properties.getProperty("language");
            if (lang != null) {
                for (int i = 0; i < languageBox.getItemCount(); i++) {
                    if (languageBox.getItemAt(i).equals(lang)) {
                        languageBox.setSelectedIndex(i);
                    }
                }
            }

            contentPanel.add(languageBox, "4, 2, fill, default");
        }
        {
            JTextPane txtpnWhenChanging = new JTextPane();
            txtpnWhenChanging.setBackground(SystemColor.control);
            txtpnWhenChanging.setText(LANGUAGE
                    .getString("ClientSettingsDialog.txtpnWhenChanging.text")); //$NON-NLS-1$
            contentPanel.add(txtpnWhenChanging, "4, 4, fill, fill");
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton(
                        LANGUAGE.getString("ClientSettingsDialog.okButton.text")); //$NON-NLS-1$
                okButton.setAction(saveSettingsAction);
                okButton.setActionCommand(LANGUAGE
                        .getString("ClientSettingsDialog.okButton.actionCommand")); //$NON-NLS-1$
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                JButton cancelButton = new JButton(
                        LANGUAGE.getString("ClientSettingsDialog.cancelButton.text")); //$NON-NLS-1$
                cancelButton.setAction(cancelAction);
                cancelButton
                        .setActionCommand(LANGUAGE
                                .getString("ClientSettingsDialog.cancelButton.actionCommand")); //$NON-NLS-1$
                buttonPane.add(cancelButton);
            }
        }
    }

    public JComboBox getLanguageBox () {
        return languageBox;
    }

    private class SaveSettingsAction extends AbstractAction {
        public SaveSettingsAction () {
            putValue(NAME, LANGUAGE
                    .getString("ClientSettingsDialog.okButton.actionCommand"));
        }

        public void actionPerformed (ActionEvent e) {
            clientWindow.properties.setProperty("language", getLanguageBox()
                    .getSelectedItem().toString());

            if (!clientWindow.properties.saveSettings()) {
                JOptionPane
                        .showMessageDialog(
                                ClientSettingsDialog.this,
                                LANGUAGE.getString("ConnectDialog.message.savesettings.text"),
                                LANGUAGE.getString("ConnectDialog.message.savesettings.title"),
                                JOptionPane.WARNING_MESSAGE);
            }
            setVisible(false);
            dispose();
        }
    }

    private class CancelAction extends AbstractAction {
        public CancelAction () {
            putValue(
                    NAME,
                    LANGUAGE.getString("ClientSettingsDialog.cancelButton.text"));
        }

        public void actionPerformed (ActionEvent e) {
            setVisible(false);
            dispose();
        }
    }
}
