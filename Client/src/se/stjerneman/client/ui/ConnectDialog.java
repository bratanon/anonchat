package se.stjerneman.client.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import org.imgscalr.Scalr;

import se.stjerneman.client.Client;
import se.stjerneman.client.ui.utils.ApplicationIcons;
import se.stjerneman.common.user.ClientUser;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author Emil Stjerneman
 * 
 */
public class ConnectDialog extends JDialog {
    private static final ResourceBundle LANGUAGE = ResourceBundle
            .getBundle("se.stjerneman.client.ui.lang"); //$NON-NLS-1$

    private static final long serialVersionUID = -4911605103198889817L;

    private ClientWindow clientWindow;

    private final JTextField username;
    private final JTextField hostPort;
    private final JTextField hostIP;
    private JCheckBox saveSettings;
    private JLabel lblImage;
    private JLabel lblImagePlaceholder;
    private JButton btnImageButton;
    private final Action imageLoadAction = new ImageLoadAction();
    private JPanel panel;
    private final JPanel panel_1 = new JPanel();
    private JPanel panel_2;
    private String imagePath;
    private ImageIcon imageIcon;
    private JButton btnConnect;

    /**
     * Create the dialog.
     * 
     * @param clientWindow
     *            a ClientWindow object.
     */
    public ConnectDialog (ClientWindow clientWindow) {
        this.clientWindow = clientWindow;
        setMinimumSize(new Dimension(280, 175));
        setSize(new Dimension(301, 229));
        setLocationRelativeTo(null);
        setIconImages(ApplicationIcons.getIcons());
        setTitle(LANGUAGE.getString("ConnectDialog.this.title")); //$NON-NLS-1$
        // setAlwaysOnTop(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setModalityType(ModalityType.APPLICATION_MODAL);

        getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
                FormFactory.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("78px"),
                FormFactory.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("89px:grow"),
                FormFactory.RELATED_GAP_COLSPEC, },
                new RowSpec[] {
                        FormFactory.RELATED_GAP_ROWSPEC,
                        RowSpec.decode("20px"),
                        FormFactory.LINE_GAP_ROWSPEC,
                        RowSpec.decode("20px"),
                        FormFactory.LINE_GAP_ROWSPEC,
                        RowSpec.decode("20px"),
                        FormFactory.LINE_GAP_ROWSPEC,
                        RowSpec.decode("fill:default:grow"),
                        FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, }));

        hostIP = new JTextField();
        hostIP.setName(LANGUAGE.getString("ConnectDialog.hostIP.name")); //$NON-NLS-1$
        hostIP.setText(clientWindow.properties.getProperty("hostIP"));
        getContentPane().add(hostIP, "4, 2, fill, fill");
        hostIP.setColumns(10);

        hostPort = new JTextField();
        hostPort.setName(LANGUAGE.getString("ConnectDialog.hostPort.name")); //$NON-NLS-1$
        hostPort.setText(clientWindow.properties.getProperty("hostPort"));
        getContentPane().add(hostPort, "4, 4, fill, fill");
        hostPort.setColumns(10);

        username = new JTextField();
        username.setName(LANGUAGE.getString("ConnectDialog.username.name")); //$NON-NLS-1$
        getContentPane().add(username, "4, 6, fill, fill");
        username.setColumns(10);
        username.setText(clientWindow.properties.getProperty("username"));

        JLabel lblHostIp = new JLabel(
                LANGUAGE.getString("ConnectDialog.lblHostIp.text")); //$NON-NLS-1$
        getContentPane().add(lblHostIp, "2, 2, fill, center");

        JLabel lblHostPort = new JLabel(
                LANGUAGE.getString("ConnectDialog.lblHostPort.text")); //$NON-NLS-1$
        getContentPane().add(lblHostPort, "2, 4, fill, center");

        JLabel lblUsername = new JLabel(
                LANGUAGE.getString("ConnectDialog.lblUsername.text")); //$NON-NLS-1$
        lblUsername.setLabelFor(username);
        getContentPane().add(lblUsername, "2, 6, fill, center");

        lblImage = new JLabel(LANGUAGE.getString("ConnectDialog.lblImage.text")); //$NON-NLS-1$
        getContentPane().add(lblImage, "2, 8, default, top");

        lblImagePlaceholder = new JLabel("");
        String imagePath = clientWindow.properties.getProperty("imagePath");
        if (imagePath != null) {
            File imgFile = new File(imagePath);
            String mimeType = new MimetypesFileTypeMap()
                    .getContentType(imgFile);
            if (mimeType.substring(0, 5).equalsIgnoreCase("image")
                    && imgFile.exists()) {
                new ResizeImage(imgFile).execute();
            }
        }
        getContentPane().add(panel_1, "4, 8, fill, top");
        panel_1.setLayout(new BorderLayout(0, 0));
        panel_1.add(lblImagePlaceholder);

        panel_2 = new JPanel();
        FlowLayout flowLayout = (FlowLayout) panel_2.getLayout();
        flowLayout.setVgap(0);
        flowLayout.setHgap(0);
        flowLayout.setAlignment(FlowLayout.RIGHT);
        panel_1.add(panel_2, BorderLayout.EAST);

        btnImageButton = new JButton();
        panel_2.add(btnImageButton);
        btnImageButton.setAction(imageLoadAction);

        panel = new JPanel();
        getContentPane().add(panel, "2, 12, 3, 1, fill, fill");
        panel.setLayout(new BorderLayout(0, 0));

        saveSettings = new JCheckBox(
                LANGUAGE.getString("ConnectDialog.saveSettings.text")); //$NON-NLS-1$
        panel.add(saveSettings, BorderLayout.WEST);

        btnConnect = new JButton(
                LANGUAGE.getString("ConnectDialog.btnConnect.text")); //$NON-NLS-1$
        panel.add(btnConnect, BorderLayout.EAST);
        btnConnect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                saveConfig();
                if (validateTextFields() && establishConnection()) {
                    dispose();
                }
            }
        });

    }

    @SuppressWarnings ("javadoc")
    public String getHostPort () {
        return hostPort.getText().trim();
    }

    @SuppressWarnings ("javadoc")
    public String getHostIP () {
        return hostIP.getText().trim();
    }

    @SuppressWarnings ("javadoc")
    public String getUsername () {
        return username.getText().trim();
    }

    @SuppressWarnings ("javadoc")
    public JCheckBox getSaveSettings () {
        return saveSettings;
    }

    @SuppressWarnings ("javadoc")
    public JLabel getLblImagePlaceholder () {
        return lblImagePlaceholder;
    }

    @SuppressWarnings ("javadoc")
    public String getImagePath () {
        return imagePath;
    }

    @SuppressWarnings ("javadoc")
    public ImageIcon getImageIcon () {
        return imageIcon;
    }

    @SuppressWarnings ("javadoc")
    public JButton getBtnConnect () {
        return btnConnect;
    }

    private boolean saveConnectionDetails () {
        clientWindow.properties.setProperty("hostIP", getHostIP());
        clientWindow.properties.setProperty("hostPort", getHostPort());
        clientWindow.properties.setProperty("username", getUsername());

        if (getImagePath() != null) {
            clientWindow.properties.setProperty("imagePath", getImagePath());
        }

        return clientWindow.properties.saveSettings();
    }

    private void saveConfig () {
        if (getSaveSettings().isSelected() && !saveConnectionDetails()) {
            JOptionPane
                    .showMessageDialog(
                            this,
                            LANGUAGE.getString("ConnectDialog.message.savesettings.text"),
                            LANGUAGE.getString("ConnectDialog.message.savesettings.title"),
                            JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * Validate text fields.
     * 
     * @return true if all fields validated, otherwise false.
     */
    private boolean validateTextFields () {
        JTextField[] fields = {
                hostIP,
                hostPort,
                username
        };

        for (JTextField field : fields) {
            if (field.getText().trim().isEmpty()) {
                JOptionPane
                        .showMessageDialog(
                                this,
                                field.getName()
                                        + LANGUAGE
                                                .getString("ConnectDialog.message.emptyfield.text"),
                                LANGUAGE.getString("ConnectDialog.message.emptyfield.title"),
                                JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }

        return true;
    }

    /**
     * Try establish a connection to the host.
     * 
     * @return true when connected successfully, otherwise false.
     */
    private boolean establishConnection () {
        Client client = Client.getInstance();
        try {
            client.connect(getHostIP(), Integer.parseInt(getHostPort()));

            clientWindow.getMenu().setConnectedMode();

            ClientUser user = new ClientUser();
            user.setUsername(getUsername());
            user.setImage(getImageIcon());
            client.sendUser(user);

            clientWindow.startListen();
            return true;
        }
        catch (IllegalArgumentException e) {
            JOptionPane
                    .showMessageDialog(
                            this,
                            LANGUAGE.getString("ConnectDialog.message.connectionfail.username"),
                            LANGUAGE.getString("ConnectDialog.message.connectionfail.title"),
                            JOptionPane.WARNING_MESSAGE);
        }
        catch (UnknownHostException e) {
            JOptionPane
                    .showMessageDialog(
                            this,
                            LANGUAGE.getString("ConnectDialog.message.connectionfail.unknownhost"),
                            LANGUAGE.getString("ConnectDialog.message.connectionfail.title"),
                            JOptionPane.WARNING_MESSAGE);
        }
        catch (IOException e) {
            JOptionPane
                    .showMessageDialog(
                            this,
                            LANGUAGE.getString("ConnectDialog.message.connectionfail.error"),
                            LANGUAGE.getString("ConnectDialog.message.connectionfail.title"),
                            JOptionPane.WARNING_MESSAGE);
        }

        return false;
    }

    private void setImage (ImageIcon image) {
        imageIcon = image;
        getLblImagePlaceholder().setIcon(image);
        pack();
    }

    private class ImageLoadAction extends AbstractAction {

        private static final long serialVersionUID = -8982483432146886008L;

        public ImageLoadAction () {
            putValue(NAME,
                    LANGUAGE.getString("ConnectDialog.btnImageButton.text"));
        }

        public void actionPerformed (ActionEvent e) {
            JFileChooser dialog = new JFileChooser();

            switch (dialog.showOpenDialog(null)) {
                case JFileChooser.CANCEL_OPTION:
                case JFileChooser.ERROR_OPTION:
                    return;
                case JFileChooser.APPROVE_OPTION:
                    File imgFile = dialog.getSelectedFile();
                    imagePath = imgFile.getAbsolutePath();
                    new ResizeImage(imgFile).execute();
            }
        }
    }

    private class ResizeImage extends SwingWorker<BufferedImage, Void> {
        private File imgFile;

        public ResizeImage (File imgFile) {
            this.imgFile = imgFile;
        }

        @Override
        protected BufferedImage doInBackground () throws Exception {
            getBtnConnect().setEnabled(false);
            getLblImagePlaceholder().setIcon(null);
            getLblImagePlaceholder().setText(
                    LANGUAGE.getString("ConnectDialog.imageloading"));
            BufferedImage scaledImg = null;
            try {
                BufferedImage image = ImageIO.read(imgFile);
                scaledImg = Scalr.resize(image, 200);
            }
            catch (IOException e) {}

            return scaledImg;
        };

        @Override
        protected void done () {
            try {
                getBtnConnect().setEnabled(true);
                getLblImagePlaceholder().setText("");
                setImage(new ImageIcon(this.get()));
            }
            catch (InterruptedException e) {}
            catch (ExecutionException e) {}
        }
    };

}
