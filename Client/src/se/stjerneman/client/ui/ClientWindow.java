package se.stjerneman.client.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.SocketException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import se.stjerneman.client.Client;
import se.stjerneman.client.ui.properties.ClientProperties;
import se.stjerneman.client.ui.utils.ApplicationIcons;
import se.stjerneman.common.messages.Message;
import se.stjerneman.common.user.ClientUser;

/**
 * 
 * @author Emil Stjerneman
 * 
 * 
 */
public class ClientWindow {
    private static final ResourceBundle LANGUAGE = ResourceBundle
            .getBundle("se.stjerneman.client.ui.lang"); //$NON-NLS-1$

    private static final Logger logger = Logger.getLogger(ClientWindow.class
            .getName());

    /**
     * A client object.
     */
    protected Client client;

    /**
     * A list of properties this client has.
     */
    protected ClientProperties properties;

    protected ClientWindowMenu menu;

    private JFrame frame;
    private JButton btnSend;
    private JTextArea messageTextArea;
    private JList<ClientUser> clientList;
    private DefaultListModel<ClientUser> clientListModel;
    private JTextPane chatLogTextPane;
    private JScrollPane clientListScrollPane;

    private final Action sendMessageAction = new SendMessageAction();

    private JPopupMenu userPopUp;

    /**
     * Launch the application.
     * 
     * @param args
     */
    public static void main (String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run () {
                try {
                    String UIName = UIManager.getSystemLookAndFeelClassName();
                    UIManager.setLookAndFeel(UIName);

                    ClientWindow window = new ClientWindow();
                    window.frame.pack();
                    window.frame.setLocationRelativeTo(null);
                    window.frame.setVisible(true);

                    ConnectDialog cd = new ConnectDialog(window);
                    cd.pack();
                    cd.setVisible(true);

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public ClientWindow () {

        BasicConfigurator.configure();
        properties = new ClientProperties();
        String lang = properties.getProperty("language");
        if (lang != null) {
            Locale.setDefault(new Locale(lang));
        }
        client = Client.getInstance();
        menu = new ClientWindowMenu(this);
        initialize();
    }

    @SuppressWarnings ("javadoc")
    public JButton getBtnSend () {
        return btnSend;
    }

    @SuppressWarnings ("javadoc")
    public JTextArea getMessageTextArea () {
        return messageTextArea;
    }

    @SuppressWarnings ("javadoc")
    public DefaultListModel<ClientUser> getClientListModel () {
        return clientListModel;
    }

    @SuppressWarnings ("javadoc")
    public JList<ClientUser> getClientList () {
        return clientList;
    }

    @SuppressWarnings ("javadoc")
    public JTextPane getChatLogTextPane () {
        return chatLogTextPane;
    }

    @SuppressWarnings ("javadoc")
    public JScrollPane getClientListScrollPane () {
        return clientListScrollPane;
    }

    @SuppressWarnings ("javadoc")
    public ClientWindowMenu getMenu () {
        return menu;
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize () {
        frame = new JFrame();
        frame.setIconImages(ApplicationIcons.getIcons());
        frame.setJMenuBar(menu.getMenuBar());
        frame.setTitle(LANGUAGE.getString("ClientWindow.frame.title")); //$NON-NLS-1$
        frame.setBounds(100, 100, 450, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(800, 600));
        frame.setPreferredSize(new Dimension(800, 600));

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing (WindowEvent e) {
                Client.getInstance().disconnect();
                System.exit(0);
            }
        });

        JSplitPane mainSplitPane = new JSplitPane();
        mainSplitPane.setResizeWeight(1.0);
        frame.getContentPane().add(mainSplitPane, BorderLayout.CENTER);

        JSplitPane logSplitPane = new JSplitPane();
        logSplitPane.setResizeWeight(1.0);
        logSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
        mainSplitPane.setLeftComponent(logSplitPane);

        JScrollPane chatLogScrollPane = new JScrollPane();
        chatLogScrollPane
                .setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        chatLogScrollPane.setMinimumSize(new Dimension(100, 100));
        logSplitPane.setLeftComponent(chatLogScrollPane);

        chatLogTextPane = new JTextPane();
        chatLogTextPane.setEnabled(false);
        chatLogTextPane.setEditable(false);
        chatLogScrollPane.setViewportView(chatLogTextPane);

        JPanel sendPanel = new JPanel();
        sendPanel.setMinimumSize(new Dimension(10, 95));
        sendPanel.setPreferredSize(new Dimension(10, 95));
        logSplitPane.setRightComponent(sendPanel);
        sendPanel.setLayout(new BorderLayout(0, 0));

        JScrollPane messageScrollPane = new JScrollPane();
        messageScrollPane
                .setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        sendPanel.add(messageScrollPane, BorderLayout.CENTER);

        messageTextArea = new JTextArea();
        messageTextArea.setEditable(false);
        messageTextArea.setEnabled(false);
        messageTextArea.setMinimumSize(new Dimension(4, 60));
        messageTextArea.setRows(3);
        messageTextArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed (KeyEvent e) {

                if (e.isShiftDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {
                    messageTextArea.append("\n");
                }
                else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    sendMessage();
                    e.consume();
                }
            }
        });

        messageScrollPane.setViewportView(messageTextArea);

        JPanel buttonPanel = new JPanel();
        sendPanel.add(buttonPanel, BorderLayout.SOUTH);
        buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

        btnSend = new JButton(LANGUAGE.getString("ClientWindow.btnSend.text"));
        btnSend.setEnabled(false);
        btnSend.addActionListener(sendMessageAction);
        buttonPanel.add(btnSend);

        clientListScrollPane = new JScrollPane();
        clientListScrollPane.setPreferredSize(new Dimension(150, 2));
        clientListScrollPane
                .setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        clientListScrollPane
                .setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        clientListScrollPane.setMinimumSize(new Dimension(100, 10));
        mainSplitPane.setRightComponent(clientListScrollPane);

        clientListModel = new DefaultListModel<ClientUser>();
        clientList = new JList<ClientUser>();
        clientList.setVisibleRowCount(-1);
        clientList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        clientList.setPreferredSize(new Dimension(100, 100));
        clientList.setModel(clientListModel);

        clientListScrollPane.setViewportView(clientList);

        clientList.addMouseListener(new MouseAdapter() {
            public void mousePressed (MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    clientList.setSelectedIndex(clientList.locationToIndex(e
                            .getPoint()));
                }
            }

            @Override
            public void mouseReleased (MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    if (clientList.getSelectedValue() != null) {
                        ClientUser user = (ClientUser) clientList
                                .getSelectedValue();
                        userPopUp.show(clientList, e.getX(), e.getY());
                    }
                }
            }
        });

        JMenuItem showInfo = new JMenuItem(
                LANGUAGE.getString("ClientWindow.mntmShowInfo.text"));
        showInfo.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent event) {
                ClientUser user = clientList.getSelectedValue();
                UserInfoDialog dialog = new UserInfoDialog(user);
                dialog.setVisible(true);
                dialog.pack();
            }
        });
        userPopUp = new JPopupMenu();
        userPopUp.add(showInfo);
        // userPopUp.addSeparator();
        // userPopUp.add(new JMenuItem(LANGUAGE
        // .getString("ClientWindow.mntmKick.text")));
        // userPopUp.addSeparator();

    }

    protected void startListen () {
        new Thread(new IncomingListener()).start();
    }

    private void sendMessage () {
        getMessageTextArea().setEnabled(false);
        String message = getMessageTextArea().getText();

        if (message.isEmpty()) {
            getMessageTextArea().setEnabled(true);
            return;
        }

        getMessageTextArea().setText("");

        client.sendMessage(message);

        getMessageTextArea().setEnabled(true);
        getMessageTextArea().requestFocusInWindow();
    }

    /**
     * 
     * @author Emil
     * 
     */
    private class SendMessageAction extends AbstractAction {

        private static final long serialVersionUID = -8905572626321773564L;

        public SendMessageAction () {
            putValue(NAME, LANGUAGE.getString("ClientWindow.btnSend.text"));
        }

        public void actionPerformed (ActionEvent e) {
            sendMessage();
        }
    }

    /**
     * Listen for incoming packets from the server.
     * 
     * @author Emil Stjerneman
     * 
     */
    private class IncomingListener implements Runnable {
        JTextPane chatLog = getChatLogTextPane();

        @Override
        public void run () {

            try {
                while (!client.getSocket().isClosed()) {
                    byte sentByte = client.getInputStream().readByte();

                    switch (sentByte) {
                        case 1:
                            catchMessageObject();
                            break;
                        case 2:
                            catchUserListObject();
                            break;
                    }
                }
            }
            catch (SocketException e) {
                getMenu().setDisconnectedMode();
                JOptionPane
                        .showMessageDialog(
                                null,
                                LANGUAGE.getString("ClientWindow.message.disconnected"),
                                "", JOptionPane.INFORMATION_MESSAGE);
            }

            catch (ClassNotFoundException e) {}
            catch (IOException e) {}
        }

        private void catchMessageObject () throws ClassNotFoundException,
                IOException {
            Message msg = (Message) client.getInputStream().readObject();

            StyledDocument doc = chatLog.getStyledDocument();

            try {
                String text = "\n" + msg.formatMessage();
                doc.insertString(doc.getLength(), text, msg.getStyle(doc));
            }
            catch (BadLocationException e) {}

            chatLog.setCaretPosition(doc.getLength());
        }

        private void catchUserListObject () throws ClassNotFoundException,
                IOException {

            @SuppressWarnings ("unchecked")
            List<ClientUser> clients = (List<ClientUser>) client
                    .getInputStream().readObject();

            clientListModel.clear();

            for (ClientUser user : clients) {
                clientListModel.addElement(user);
            }
        }
    }
}
