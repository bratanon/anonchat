package se.stjerneman.client.ui.properties;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Handle client properties.
 * 
 * Default properties will be overridden by the same properties in the property
 * file.
 * 
 * @author Emil Stjerneman
 * 
 */
public class ClientProperties {

    private static final Logger logger = Logger
            .getLogger(ClientProperties.class.getName());

    /**
     * A property list that contains client properties.
     */
    private static Properties properties;

    /**
     * The file that could contain properties.
     */
    private static File propertyFile;

    /**
     * 
     */
    public ClientProperties () {
        propertyFile = new File("config.properties");
        initProperties();
    }

    /**
     * Searches for the property with the specified key in this property list.
     * 
     * @param key
     *            the property key.
     * @return the value in this property list with the specified key value.
     */
    public String getProperty (String key) {
        return ClientProperties.properties.getProperty(key);
    }

    /**
     * Sets the property with the specified key to the specified value in the
     * property list.
     * 
     * @param key
     *            the key to be placed into this property list.
     * @param value
     *            the value corresponding to key.
     */
    public void setProperty (String key, String value) {
        ClientProperties.properties.setProperty(key, value);
    }

    /**
     * Initialize properties.
     */
    private void initProperties () {
        ClientProperties.properties = new Properties();
        setDefaultPoperties();

        if (propertyFile.exists()) {
            loadPropertiesFromFile();
        }
        else {
            saveSettings();
        }
    }

    /**
     * Reads a property list from the property file.
     */
    private void loadPropertiesFromFile () {
        try {
            ClientProperties.properties.load(new FileReader(propertyFile));
            logger.info("Properties from config.properties loaded.");
        }
        catch (IOException e) {
            logger.error("Couldn't load the config.properties file.", e);
        }
    }

    /**
     * Stores this property list to a property file.
     * 
     * @return true on success, otherwise false.
     */
    public boolean saveSettings () {
        try {
            ClientProperties.properties.store(new FileWriter(propertyFile),
                    "AnonChat UI config properties.");
            logger.info("Properties saved to config.properties.");
            return true;
        }
        catch (IOException e) {
            logger.error("Couldn't save the config.properties file.", e);
            return false;
        }
    }

    /**
     * Sets default properties.
     */
    private void setDefaultPoperties () {
        ClientProperties.properties.setProperty("hostIP", "127.0.0.1");
        ClientProperties.properties.setProperty("hostPort", "27015");
    }
}
